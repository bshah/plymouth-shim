# plymouth-shim

This is implementation of plymouth without it being actual bootsplash provider

# Why?

Main problem this aims to solve is packagekit have a hard-coded calls to plymouth which can not work in Plasma Mobile because we do not use plymouth there

# What?

This provides a simple QML app which can be run with eglfs plugin using systemd service which binds to packagekit offline upgrade service, and shows a friendly message that system is updating, please wait and do not restart your phone(!)

# Install instructions

Build it using normal cmake workflow, install, and in post-install script of your packaging, make following symlink which points to installed plymouth-shim

```
lrwxrwxrwx 1 root root 45 Nov 30 07:40 /usr/lib/systemd/system/system-update.target.wants/plymouth-shim.service -> /usr/lib/systemd/system/plymouth-shim.service
```

If you package this in distribution, this should conflict with plymouth package.
